// header files 
#include <GL/freeglut.h>

// gobal variable declarations
float HAPPY_XPOS = 1.0f;
float BD_XPOS    = 1.0f;
float J_XPOS     = 1.0f;

void Draw_Happy(void)
{
	glLoadIdentity();

	//glRasterPos3f(-0.5f, 0.3f, 0.0f);
	glRasterPos3f(HAPPY_XPOS, 0.3f, 0.0f);
    //glColor3f(1.0f, 1.0f, 0.0f);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'H');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'A');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'P');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'P');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'Y');

	glEnd();
}
void Draw_BirthDay(void)
{
	glLoadIdentity();

//	glRasterPos3f(-0.2f, 0.0f, 0.0f);
	glRasterPos3f(BD_XPOS, 0.0f, 0.0f);
//	glColor3f(1.0, 1.0f, 0.0f);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'B');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'I');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'R');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'T');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'H');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'D');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'A');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'Y');
	glEnd();
}

void Draw_Jeswin(void)
{
	glLoadIdentity();
	
//	glRasterPos3f(0.1f, -0.3f, 0.0f);
	glRasterPos3f(J_XPOS, -0.3f, 0.0f);
	//glColor3f(1.0f, 1.0f, 1.0f);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'J');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'S');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'W');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'I');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'N');

	glEnd();
}
