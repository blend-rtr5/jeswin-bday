target		= jeswin

BUILD_DIR 	= build
SRC_DIRS	= src
INC_DIRS 	= include

SRCS = $(shell find $(SRC_DIRS) -name '*.cpp')
OBJS = $(SRCS:%.cpp=$(BUILD_DIR)/%.o)

INC_FLAGS := $(addprefix -I,$(INC_DIRS))
LD_FLAGS  = -lGL -lglut -lGLU
CPP_FLAGS = $(INC_FLAGS)

all: execute

execute: $(target)
	./$(target)

$(target): $(OBJS)
	g++ -o $@ $^ $(LD_FLAGS) $(CPP_FLAGS) $(CXXFLAGS)

$(BUILD_DIR)/%.o: %.cpp
	@mkdir -p $(dir $@)
	g++ $(CPP_FLAGS) $(CXXFLAGS) -o $@ -c $<


clean:
	rm $(OBJS) $(target)


