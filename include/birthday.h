#ifndef _BIRTHDAY_H
#define _BIRTHDAY_H
#include "geometry.h"

void drawBirthdayBackground(void);
void drawCircle(double, double, double, int, int);
void drawRectangle(float, float, float, float);
void Display_Scene_One();
void drawCurtain_circle(double, double, double, int, int);
void drawAeroplane(point_t origin, const unsigned int t);
void drawShapeWithColor(point_t* pointList, int N, COLOR color, point_t offset);
void Draw_Happy(void);
void Draw_BirthDay(void);
void Draw_Jeswin(void);
void drawCake(point_t);

#endif
