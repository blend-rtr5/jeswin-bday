#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <GL/gl.h>
#include <cmath>
typedef struct Point
{
    float x;
    float y;
} point_t;

typedef struct
{
    int r;
    int g;
    int b;
} COLOR;

/**
 * @brief Value of PI
 */
const float PI = 2 * std::acos(0.0);

/**
 * @brief Get angle in radians
 *
 * @param degrees [in] - angle in degrees
 * @return value of angle in radians
 */
inline float getRadian(int degrees) { return ((PI / 180) * degrees); }

/*--------------------------------------------------------------
 *  Baloon related functions and declaration
 * -----------------------------------------------------------*/
#define BALOON_Y_TRAVEL 0.25f

typedef struct
{
    point_t origin;   /**< origin */
    float rx;
    float ry;
    point_t stringBase;
    float yTravel;    /**< vertical movement */
    float startAngle; /**< starting vertical position i.e. 0.0->center, 90->bottom */
    GLfloat color[3]; /**< color */
} baloon_t;

/**
 * @brief Create animation for a single baloon
 * The ballon will oscillate from point->y + yTravel to
 * point->y - yTravel.
 *
 * at t = 0: ballon will start from top position and move to bottom position
 * at t = 30: baloon will start moving in reverse direction, i.e. bottom->top
 *
 * @param point [in] - center of baloon
 * @param t [in] - value of time
 */
void animateBaloon(const baloon_t* pBaloon, const unsigned int t);

/**
 * @brief draws a baloon around provided origin
 *
 * @param origin [in] - co-ordinates of origin
 */
void drawBaloon(const baloon_t* pBaloon, const point_t* origin);

void drawString(const baloon_t* pBaloon, const point_t * origin);

/**
 * @brief Draw an ellipse with given parameters
 *
 * @param pCenter [in] - center of ellipse
 * @param rx [in] - radius in x axis
 * @param ry [in] - radius in y axis
 * @param num_segments [in] - number of segments
 */
void DrawEllipse(const point_t* pCenter, float rx, float ry, int num_segments);

#endif // !GEOMETRY_H
