#include <GL/freeglut.h>
#include <iostream>
#include <math.h>
#include "birthday.h"
#include "geometry.h"

#define ZOOM 0.3f
#define X_SCALE 70.0f
#define Y_SCALE 80.0f
#define ANIMATION_SPEED 0.5f

#define lengthOf(arr) (sizeof(arr) / sizeof(arr[0]))

#define drawShape(arr, color, offset) (drawShapeWithColor(arr, lengthOf(arr), color, offset))

#define covertColor(gradient) (gradient / 256.0f)

#define colorVertex(color) (glColor3f(covertColor(color.r), covertColor(color.g), covertColor(color.b)))

#define toRadians(degree) ((22.0f * degree) / (7 * 180))

void twoDimVertex(int x, int y, point_t offset)
{
    // code
    glVertex3f(                                   // calculate coords
        (((x - 30) * ZOOM) / X_SCALE) + offset.x, // x
        ((y * ZOOM) / Y_SCALE) + offset.y,        // y
        0.0f);                                    // z
}

void pointOnCircle(int radius, point_t center, int angle, point_t offset)
{
    // coment
    twoDimVertex(radius * sin(toRadians(angle)) + center.x, radius * cos(toRadians(angle)) + center.y, offset);
}

// colors of aeroplane color code be used from 256 scale

COLOR WING      = { 218, 165, 32 };
COLOR TAIL      = { 192, 192, 192 };
COLOR BODY      = { 256, 256, 256 };
COLOR LOWERHALF = { 47, 79, 79 };
COLOR BOARDER   = { 0, 0, 0 };
COLOR POSTER    = { 245, 255, 250 };
COLOR HAPPY     = { 25, 25, 112 };
COLOR BIRTHDAY  = { 25, 25, 112 };
COLOR JESWIN    = { 25, 25, 112 };

//
point_t upperHalf[]    = { { -40, 0 }, { -30, 20 }, { -20, 30 }, { 0, 35 }, { 10, 34 }, { 80, 17 }, { 75, 0 } };
point_t bottomHalf[]   = { { 75, 0 }, { 78, -7 }, { 0, -24 }, { -20, -20 }, { -35, -17 }, { -35, -17 }, { -40, 0 } };
point_t upperTail[]    = { { 75, 0 }, { 80, 17 }, { 99, 10 }, { 99, 0 } };
point_t lowerTail[]    = { { 75, 0 }, { 99, 0 }, { 78, -7 } };
point_t WingFront[]    = { { 10, 0 }, { 22, 8 }, { 40, 0 }, { 50, -29 }, { 40, -39 } };
point_t TailWingSide[] = { { 78, -7 }, { 75, 0 }, { 99, 0 }, { 99, -15 }, { 98, -18 }, { 90, -20 } };
point_t TailWingTop[]  = { { 99, 10 }, { 90, 7 }, { 80, 17 }, { 99, 30 }, { 115, 30 } };
point_t TailWindow1[]  = { { -30, 20 }, { -20, 30 }, { 0, 35 }, { 10, 34 }, { 0, 20 } };

point_t Poster[] = { { 115, 5 }, { 250, 5 }, { 250, -20 }, { 115, -20 } };

void drawAeroplane(point_t origin, const unsigned int t)
{
    // method prototypes
    void drawShapeWithColor(point_t[], int, COLOR, point_t);
    void drawCircle(float, point_t, COLOR, point_t);
    void Draw_Happy1(int x, int y, point_t offset);
    void Draw_BirthDay(int x, int y, point_t offset);
    void Draw_Jeswin(int x, int y, point_t offset);

    // code
    origin.x = origin.x - (t / X_SCALE) * ANIMATION_SPEED;
    if (origin.x < -1.5f) return;
    drawShape(upperHalf, BODY, origin);
    drawShape(bottomHalf, LOWERHALF, origin);
    drawShape(upperTail, TAIL, origin);
    drawShape(WingFront, WING, origin);
    drawShape(TailWingSide, WING, origin);
    drawShape(TailWingTop, WING, origin);
    drawShape(TailWindow1, WING, origin);
    drawShape(Poster, POSTER, origin);

    point_t center = { 25, 20 };
    drawCircle(2.5f, center, WING, origin);

    point_t center1 = { 35, 18 };
    drawCircle(2.3f, center1, WING, origin);

    point_t center2 = { 45, 17.5f };
    drawCircle(2.1f, center2, WING, origin);

    colorVertex(HAPPY);
    Draw_Happy1(125, (t % 5 == 0) ? -10 : -11, origin);
    colorVertex(BIRTHDAY);
    Draw_BirthDay(155, (t % 5 == 0) ? -11 : -10, origin);
    colorVertex(JESWIN);
    Draw_Jeswin(200, (t % 5 == 0) ? -14 : -13, origin);
    glEnd();
}

void drawShapeWithColor(point_t* pointList, int N, COLOR color, point_t offset)
{
    // variable declarations
    int i;

    // code
    glBegin(GL_POLYGON);

    colorVertex(color);
    for (i = 0; i < N; i++) { twoDimVertex(pointList[i].x, pointList[i].y, offset); }
    glEnd();
}

void drawCircle(float radius, point_t center, COLOR color, point_t offset)
{
    // variable declarations
    // code
    colorVertex(color);
    glBegin(GL_POLYGON);
    for (int i = 0; i < 360; i++) { pointOnCircle(radius, center, i, offset); }
    glEnd();
}

void setRaster(int x, int y, point_t origin)
{
    glRasterPos3f(                                // raster position
        (((x - 30) * ZOOM) / X_SCALE) + origin.x, // x
        ((y * ZOOM) / Y_SCALE) + origin.y,        // y
        0.0f);
}

void Draw_Happy1(int x, int y, point_t origin)
{
    setRaster(x, y, origin);
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'H');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'A');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'P');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'P');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'Y');

    glEnd();
}

void Draw_BirthDay(int x, int y, point_t origin)
{
    setRaster(x, y, origin);
    //	glColor3f(1.0, 1.0f, 0.0f);
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'B');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'I');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'R');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'T');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'H');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'D');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'A');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'Y');
    glEnd();
}

void Draw_Jeswin(int x, int y, point_t origin)
{
    setRaster(x, y, origin);
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'J');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'S');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'W');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'I');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'N');

    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ' ');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, '!');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, '!');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, '!');
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, '!');

    glEnd();
}
