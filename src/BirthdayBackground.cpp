// Header files
#include <GL/freeglut.h>
#include <math.h>
#include "geometry.h"
#include "birthday.h"

// Draw Birthday Background (Rainbow, Clouds, Trees, Ground)
void drawBirthdayBackground(void)
{
	//Function declarations
	//void drawCircle(double, double, double, int, int);
	//void drawRectangle(float, float, float, float);

	// Code
	// Rainbow
	glColor3f(1.0f, 0.0f, 0.0f);
	drawCircle(1.0, 0.0, 0.0, 0, 150);
	glColor3f(1.0f, 0.5f, 0.0f);
	drawCircle(0.95, 0.0, 0.0, 0, 150);
	glColor3f(1.0f, 1.0f, 0.0f);
	drawCircle(0.9, 0.0, 0.0, 0, 150);
	glColor3f(0.0f, 1.0f, 0.0f);
	drawCircle(0.85, 0.0, 0.0, 0, 150);
	glColor3f(0.0f, 0.5f, 1.0f);
	drawCircle(0.8, 0.0, 0.0, 0, 150);
	glColor3f(0.1f, 0.0f, 24.0f);
	drawCircle(0.75, 0.0, 0.0, 0, 150);
	glColor3f(2.0f, 0.5f, 1.0f);
	drawCircle(0.7, 0.0, 0.0, 0, 150);
	glColor3f(184.0f / 255.0f, 213.0f / 255.0f, 238.0f / 255.0f);
	drawCircle(0.65, 0.0, 0.0, 0, 150);

	// Clouds
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.1, 0.6, 0.0, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.2, 0.75, 0.0, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.1, 0.65, 0.1, 0, 300);

	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.1, 0.8, 0.0, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.2, 0.95, 0.0, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.1, 0.85, 0.1, 0, 300);

	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.1, -0.6, 0.0, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.2, -0.75, 0.0, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.1, -0.65, 0.1, 0, 300);

	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.1, -0.8, 0.0, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.2, -0.95, 0.0, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawCircle(0.1, -0.85, 0.1, 0, 300);

	// Ground
	glColor3f(0.0f, 1.0f, 0.0f);
	drawRectangle(-1.0f, -0.8f, 1.0f, -1.0f);
	drawCircle(0.5, -0.85f, -1.2f, 0, 300);
	drawCircle(0.5, -0.45f, -1.2f, 0, 300);
	drawCircle(0.5, 0.85f, -1.2f, 0, 300);
	drawCircle(0.5, 0.45f, -1.2f, 0, 300);
	drawCircle(0.42, 0.0f, -1.2f, 0, 300);

	// Trees
	// Right-Side Trees
	glColor3f(0.5f, 0.0f, 0.0f);
	drawRectangle(0.56f, -0.95f, 0.52f, -0.75f);
	glColor3f(0.0f, 0.6f, 0.0f);
	drawCircle(0.08, 0.535f, -0.672f, 0, 300);
	drawCircle(0.075, 0.515f, -0.6f, 0, 300);
	drawCircle(0.08, 0.555f, -0.6f, 0, 300);
	drawCircle(0.08, 0.535f, -0.55f, 0, 300);
	drawCircle(0.04, 0.535f, -0.49f, 0, 300);

	glColor3f(0.5f, 0.0f, 0.0f);
	drawRectangle(0.9f, -0.85f, 0.86f, -0.65f);
	glColor3f(0.0f, 0.6f, 0.0f);
	drawCircle(0.08, 0.875f, -0.6f, 0, 300);
	drawCircle(0.075, 0.845f, -0.51f, 0, 300);
	drawCircle(0.08, 0.895f, -0.51f, 0, 300);
	drawCircle(0.08, 0.875f, -0.45f, 0, 300);
	drawCircle(0.04, 0.875f, -0.39f, 0, 300);

	// Left-Side Trees
	glColor3f(0.5f, 0.0f, 0.0f);
	drawRectangle(-0.9f, -0.85f, -0.86f, -0.65f);
	glColor3f(0.0f, 0.6f, 0.0f);
	drawCircle(0.08, -0.875f, -0.6f, 0, 300);
	drawCircle(0.075, -0.845f, -0.51f, 0, 300);
	drawCircle(0.08, -0.895f, -0.51f, 0, 300);
	drawCircle(0.08, -0.875f, -0.45f, 0, 300);
	drawCircle(0.04, -0.875f, -0.39f, 0, 300);

	glColor3f(0.5f, 0.0f, 0.0f);
	drawRectangle(-0.56f, -0.95f, -0.52f, -0.75f);
	glColor3f(0.0f, 0.6f, 0.0f);
	drawCircle(0.08, -0.535f, -0.672f, 0, 300);
	drawCircle(0.075, -0.515f, -0.6f, 0, 300);
	drawCircle(0.08, -0.555f, -0.6f, 0, 300);
	drawCircle(0.08, -0.535f, -0.55f, 0, 300);
	drawCircle(0.04, -0.535f, -0.49f, 0, 300);

}

// Draw Rainbow, Cloud, Ground and Tree Circles
void drawCircle(double radius, double ori_x, double ori_y, int startpoint, int endpoint) // Function Definition Of Birthday Background Circle Function
{
	glBegin(GL_POLYGON);
	for (int i = startpoint; i <= endpoint; i++)
	{
		double angle = 2 * PI * i / 300;
		double x = cos(angle) * radius;
		double y = sin(angle) * radius;
		glVertex3f(ori_x + x, ori_y + y, 0.0);
	}
	glEnd();
}

// Draw Ground and Tree Rectangles
void drawRectangle(float x, float y, float x1, float y1) // Function Definition Of Birthday Background Rectangle Function
{
	glBegin(GL_QUADS);

	glVertex3f(x, y, 0.0f);
	glVertex3f(x1, y, 0.0f);
	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x, y1, 0.0f);

	glEnd();
}
