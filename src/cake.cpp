
#include <GL/freeglut.h>
#include "geometry.h"

#define CAKE_ZOOM 0.5f
point_t cake_offset;

void drawCake(point_t origin)

{
    cake_offset.x = origin.x;
    cake_offset.y = origin.y;
    glLoadIdentity();
    glPointSize(10.0f);
    glBegin(GL_POINTS);
    glColor3f(1.0f, 0.50f, 0.0f);
    glVertex3f(0.08f, 0.25f, 0.0f);
    glVertex3f(0.02f, 0.25f, 0.0f);
    glEnd();

    // candles stick
    glLoadIdentity();
    glTranslatef(-0.00f, 0.25f, -0.05f);
    glLineWidth(5.0);
    glBegin(GL_LINES);
    glColor3f(1.0f, 1.0f, 1.0f);

    glVertex3f(0.08f, -0.01f, 0.0f);
    glVertex3f(0.08f, -0.13f, 0.0f);
    glVertex3f(0.02f, -0.01f, 0.0f);
    glVertex3f(0.02f, -0.13f, 0.0f);
    glEnd();
    // Top most layer of the cake

    glLoadIdentity();
    glBegin(GL_QUADS);
    glColor3f(1.0f, 0.75f, 0.79f); // pink color
    glVertex2f(0.0f, 0.15f);
    glVertex2f(0.1f, 0.15f);
    glVertex2f(0.1f, -0.3f);
    glVertex2f(0.0f, -0.3f);

    glColor3f(0.48f, 0.24f, 0.0f);

    // Middle layer of the cake
    glVertex2f(0.15f, 0.0f);
    glVertex2f(-0.05f, 0.0f);
    glVertex2f(-0.05f, -0.3f);
    glVertex2f(0.15f, -0.3f);
    // bottom layer of the cake
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex2f(0.2f, -0.3f);
    glVertex2f(-0.1f, -0.3f);
    glVertex2f(-0.1f, -0.2f);
    glVertex2f(0.2f, -0.2f);
    glEnd();
}