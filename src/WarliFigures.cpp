// header files
#include <GL/freeglut.h>
#include <math.h>
#include "birthday.h"
#include "geometry.h"


void Display_Scene_One()
{
	// function declarations
	//void drawCurtain_circle(double, double, double, int, int);

    // ***** SCENE 1 ****

    // Fig 1
	glBegin(GL_TRIANGLES);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.28f, -0.6f, 0.0f); //v1

	//glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.36f, -0.8f, 0.0f); //v2

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.23f, -0.8f, 0.0f); //v3


	// upper traingle
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.32f, -0.4f, 0.0f); //v1

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.28f, -0.6f, 0.0f); //v2

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.19f, -0.42f, 0.0f); //v3


	// Fig 2
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.28f, -0.6f, 0.0f); //v1

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.36f, -0.8f, 0.0f); //v2

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.23f, -0.8f, 0.0f); //v3

	// upper traingle
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.32f, -0.4f, 0.0f); //v1

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.28f, -0.6f, 0.0f); //v2

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.19f, -0.42f, 0.0f); //v3

	// Fig 3
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.68f, -0.6f, 0.0f); //v1

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.74f, -0.8f, 0.0f); //v2

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.61f, -0.8f, 0.0f); //v3

	// upper traingle
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.72f, -0.42f, 0.0f); //v1

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.68f, -0.6f, 0.0f); //v2

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.59f, -0.4f, 0.0f); //v3

	// Fig 4
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.68f, -0.6f, 0.0f); //v1

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.74f, -0.8f, 0.0f); //v2

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.62f, -0.8f, 0.0f); //v3

	// upper traingle
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.68f, -0.6f, 0.0f); //v1

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.74f, -0.4f, 0.0f); //v2

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.62f, -0.4f, 0.0f); //v3

	//glColor3f(1.0f, 1.0f, 1.0f);

	glEnd();

	// draw Drum
	//drawCurtain_circle(0.15, 0.0, -0.6, 0, 360);

	// ***draw neck lines***
    glLineWidth(3);
    //glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_LINES);

	// fig 1
    glVertex2f(-0.25, -0.42);
    glVertex2f(-0.24, -0.33);

	// fig 2 
	glVertex2f(0.25, -0.42);
    glVertex2f(0.24, -0.33);

	// fig 3
	glVertex2f(-0.65, -0.4);
    glVertex2f(-0.65, -0.35);

	// fig 4
	glVertex2f(0.67, -0.4);
    glVertex2f(0.67, -0.35);

    glEnd();


	// ***Neck Circles***
	// fig1
	drawCurtain_circle(0.05, -0.24, -0.33, 0, 360);

	//fig 2
	drawCurtain_circle(0.05, 0.24, -0.33, 0, 360);

	//fig 3
	drawCurtain_circle(0.05, -0.65, -0.33, 0, 360);

	//fig 3
	drawCurtain_circle(0.05, 0.67, -0.33, 0, 360);

	// *** Legs ***
	glLineWidth(3);
    //glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_LINES);
	
	// fig 1
    glVertex2f(0.26, -0.8);
    glVertex2f(0.24, -0.9);
	glVertex2f(0.24, -0.9);
    glVertex2f(0.25, -1.0);

	glVertex2f(0.33, -0.8);
    glVertex2f(0.31, -0.9);
	glVertex2f(0.31, -0.9);
    glVertex2f(0.35, -0.95);

	// fig 2
    glVertex2f(-0.26, -0.8);
    glVertex2f(-0.24, -0.9);
	glVertex2f(-0.24, -0.9);
    glVertex2f(-0.25, -1.0);

	glVertex2f(-0.33, -0.8);
    glVertex2f(-0.31, -0.9);
	glVertex2f(-0.31, -0.9);
    glVertex2f(-0.35, -0.95);

	//fig 3
    glVertex2f(-0.70, -0.8);
    glVertex2f(-0.67, -0.88);
	glVertex2f(-0.67, -0.88);
    glVertex2f(-0.70, -1.0);

    glVertex2f(-0.65, -0.8);
    glVertex2f(-0.61, -0.88);
	glVertex2f(-0.61, -0.88);
    glVertex2f(-0.65, -1.0);

	//fig 4
    glVertex2f(0.70, -0.8);
    glVertex2f(0.67, -0.88);
	glVertex2f(0.67, -0.88);
    glVertex2f(0.70, -1.0);

    glVertex2f(0.65, -0.8);
    glVertex2f(0.61, -0.88);
	glVertex2f(0.61, -0.88);
    glVertex2f(0.65, -1.0);

	// **** Hands ****

	// hand1 (fig 2)
	glVertex2f(0.32, -0.4);
    glVertex2f(0.35, -0.53);

	glVertex2f(0.35, -0.53);
    glVertex2f(0.29, -0.595);

	//hand2 (fig 2)
	glVertex2f(0.19, -0.42);
    glVertex2f(0.20, -0.55);

	glVertex2f(0.20, -0.55);
    glVertex2f(0.15, -0.5);



	// hand1 (fig 1)
	glVertex2f(-0.32, -0.4);
    glVertex2f(-0.35, -0.53);

	glVertex2f(-0.35, -0.53);
    glVertex2f(-0.29, -0.595);

	//hand2 (fig 1)
	glVertex2f(-0.19, -0.42);
    glVertex2f(-0.20, -0.55);

	glVertex2f(-0.20, -0.55);
    glVertex2f(-0.15, -0.5);

	// hand1 (fig 3)
	glVertex2f(-0.72, -0.42);
    glVertex2f(-0.79, -0.42);

	glVertex2f(-0.79, -0.42);
    glVertex2f(-0.79, -0.3);

	//hand2 (fig 3)
	glVertex2f(-0.61, -0.42);
    glVertex2f(-0.54, -0.42);

	glVertex2f(-0.54, -0.42);
    glVertex2f(-0.54, -0.55);

	// hand1 (fig 4)
	glVertex2f(0.62, -0.4);
    glVertex2f(0.59, -0.54);

	glVertex2f(0.59, -0.54);
    glVertex2f(0.64, -0.65);

	//hand2 (fig 4)
	glVertex2f(0.74, -0.4);
    glVertex2f(0.77, -0.54);

	glVertex2f(0.77, -0.54);
    glVertex2f(0.72, -0.65);

    glEnd();
}


// Funtion Used To Draw Circles
void drawCurtain_circle(double radius, double ori_x, double ori_y, int startpoint, int endpoint) // Function Definition
{
    glBegin(GL_POLYGON);
    for (int i = startpoint; i <= endpoint; i++)
    {
        double angle = 2 * PI * i / 50;
        double x = cos(angle) * radius;
        double y = sin(angle) * radius;
        glVertex3f(ori_x + x, ori_y + y, 0.0);
    }
    glEnd();
}
