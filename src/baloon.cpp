#include <GL/freeglut.h>
#include "geometry.h"
#include <GL/gl.h>
#include <math.h>

/* User defined functions */
void animateBaloon(const baloon_t* pBaloon, const unsigned int t)
{
    point_t coOrdinate = pBaloon->origin;

    /* Get smooth sinusoidal vertical motion of baloon */
    coOrdinate.y -= pBaloon->yTravel * sin(getRadian(t) + pBaloon->startAngle);
    glColor3fv(pBaloon->color);
    drawBaloon(pBaloon, &coOrdinate);
    drawString(pBaloon, &coOrdinate);
    
}

void drawBaloon(const baloon_t* pBaloon, const point_t* origin) { DrawEllipse(origin, pBaloon->rx, pBaloon->ry, 360); }

void drawString(const baloon_t* pBaloon, const point_t* origin) { 
    // coOrdinate - yTravel -> string base

	glLineWidth(3);

    glBegin(GL_LINES);
    glVertex2f(pBaloon->stringBase.x, pBaloon->stringBase.y);
    glVertex2f(origin->x, origin->y - pBaloon->ry);
    glEnd();

}

/**
 * @brief Draw an ellipse with given parameters
 *
 * @param pCenter [in] - center of ellipse
 * @param rx [in] - radius in x axis
 * @param ry [in] - radius in y axis
 * @param num_segments [in] - number of segments
 */
void DrawEllipse(const point_t* pCenter, float rx, float ry, int num_segments)
{
    float theta = 2 * 3.1415926 / float(num_segments);
    float c     = cosf(theta); // precalculate the sine and cosine
    float s     = sinf(theta);
    float t;

    float start_angle = 1; // we start at angle = 0
    float y           = 0;

    glBegin(GL_POLYGON);
    for (int ii = 0; ii < num_segments; ii++)
    {
        // apply radius and offset
        glVertex2f(start_angle * rx + pCenter->x, y * ry + pCenter->y); // output vertex

        // apply the rotation matrix
        t           = start_angle;
        start_angle = c * start_angle - s * y;
        y           = s * t + c * y;
    }
    glEnd();
}
