// header files
#include <GL/freeglut.h>
#include <cstdint>
#include <math.h>
#include <iostream>
#include <time.h>
#include <thread>
#include <mmsystem.h>
#include "geometry.h"
#include "birthday.h"

int count = 0;
#define FPS 30
baloon_t baloons[] = {
    { { 0.32, 0.0 }, 0.05, 0.1, { 0.32, -0.39 }, .1, 0.0, { 1.0, 0.0, 0.0 } },    // red baloon
    { { 0.74, 0.0 }, 0.05, 0.1, { 0.74, -0.29 }, .1, 45.0, { 0.0, 1.0, 0.0 } },   // yellow baloon
    { { -0.35, 0.0 }, 0.05, 0.1, { -0.35, -0.49 }, .1, 90.0, { 1.0, 0.0, 1.0 } },  // pink baloon
    { { -0.79, 0.0 }, 0.05, 0.1, { -0.79, -0.29 }, .1, 135.0, { 0.0, 0.0, 1.0 } }, // blue baloon
};

// globle variable declarations
bool bIsFullScreen = false;

// entry-point function
int main(int argc, char* argv[])
{
    // function declarations
    void initialize(void);
    void resize(int, int);
    void display(void);
    void keyboard(unsigned char, int, int);
    void mouse(int, int, int, int);
    void uninitialize(void);
    void setCounter(int);

    // code
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(1.0, 1.0);
    glutCreateWindow("RTR5 BLEND : Happy Birthday Jeswin");

    initialize();

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutTimerFunc(2000, setCounter, 0);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutCloseFunc(uninitialize);

    glutMainLoop();

    return (0);
}

void setCounter(int val)
{
    count++;
    // HAPPY_XPOS = HAPPY_XPOS - 0.1f;
    // BD_XPOS = HAPPY_XPOS - 0.1f;
    // J_XPOS = HAPPY_XPOS - 0.1f;

    // if (HAPPY_XPOS <= -0.5f)
    // 	HAPPY_XPOS = -0.5f;
    // if (BD_XPOS <= -0.2f)
    // 	BD_XPOS = -0.2f;

    // if (J_XPOS <= 0.1f)
    // 	J_XPOS = 0.1f;
    glutPostRedisplay();
    glutTimerFunc(1000 / FPS, setCounter, 0);
}
void initialize(void)
{
    // code
    glClearColor(184.0f / 255.0f, 213.0f / 255.0f, 238.0f / 255.0f, 1.0f);

    if (bIsFullScreen == false)
    {
        glutFullScreen();
        bIsFullScreen = true;
    }
    else
    {
        glutLeaveFullScreen();
        bIsFullScreen = false;
    }
}

void resize(int width, int height)
{
    // code
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
    point_t origin = { 1.1f, 0.5f };

    // code
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);

    drawBirthdayBackground();
    drawCake(origin);
    Display_Scene_One();
    for (uint8_t idx = 0U; idx < (sizeof(baloons) / sizeof(baloon_t)); ++idx) { animateBaloon(baloons + idx, count); }
    drawAeroplane(origin, count);

    // glColor3f(1.0f, 0.0f, 1.0f);
    // Draw_Happy();

    // glColor3f(0.0f, 1.0f, 1.0f);
    // Draw_BirthDay();

    // glColor3f(1.0f, 1.0f, 0.0f);
    // Draw_Jeswin();
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
    // code
    switch (key)
    {
    case 27: glutLeaveMainLoop(); break;
    case 'F':
    case 'f':
        if (bIsFullScreen == false)
        {
            glutFullScreen();
            bIsFullScreen = true;
        }
        else
        {
            glutLeaveFullScreen();
            bIsFullScreen = false;
        }
        break;

    case 'S':
    case 's':
        PlaySound("HAPPY BIRTHDAY INSTRUMENTAL.wav", NULL, SND_ASYNC | SND_NODEFAULT);
        break;

    default: break;
    }
}

void mouse(int button, int state, int x, int y)
{
    // code
    switch (button)
    {
    case GLUT_RIGHT_BUTTON: glutLeaveMainLoop(); break;
    default: break;
    }
}

void uninitialize(void)
{
    // code
}
